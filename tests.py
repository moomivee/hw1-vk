import pytest
from utils import *
from main import *
@pytest.fixture

def test_year():
    assert year_from_bdate(None, '01.2020') == '2020'
    assert year_from_bdate(None, '01.01.1999') ==  '1999'
    assert year_from_bdate(None, '01.03') is None
    assert year_from_bdate(None, '02.02.888') is None

def test_gender():
    assert gender_from_int('') == 'Мужчина'
    assert gender_from_int(1) ==  'Женщина'
    assert gender_from_int(2) ==  'Мужчина'

def api_query_my_profile():
    assert api_query_my_profile(None) is not None
    assert type(api_query_my_profile(None)) == list