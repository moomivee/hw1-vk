def year_from_bdate(bdate):
    """
    Extracts year from date string.

    Args:
        - bdate (str): date in the dd.mm.yyyy format.
    Returns:
        - str or None: The birth year or None.
    """
    bdate = str(bdate)
    if len(bdate.split('.')[-1]) == 4:
        return bdate.split('.')[-1]
    else:
        return None
    
def gender_from_int(gender):
    """
    Decode and transform int values into strings 

    Args:
    - gender (int): 1 or 2

    Returns:
    - str: gender of user
    """
    if gender == 1:
        return 'Женщина'
    else:
        return 'Мужчина'


