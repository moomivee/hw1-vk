import vk
import os
import pandas as pd
import numpy as np
from utils import *

class hw_wk_sample:

    def __init__(self):
        """
        Initializes the hw_wk_sample class for sampling and transforming data.

        Attributes:
        - api_url (str): The url for VK API methods.
        - api_token (str): The access token for VK API authentication.
        - api_version (str): Curent VK API version.
        - city (str): The city name for the VK API query.
        """
        self.api_url = 'https://api.vk.com/method'
        self.api_token = os.getenv('my_vk_token')
        self.api_version = '5.131'
        self.city = 'Барнаул'
        
    def api_query_my_profile(self):
        """
        Queries the VK API for my profile info

        Returns:
        - json: data with sample of users info 
        """

        return vk.API(access_token=self.api_token, v=self.api_version).users.get(user_ids='moomivee')
    
    def api_query_1000(self):
        """
        Queries the VK API for sample of users

        Returns:
        - json: data with sample of users info 
        """

        return vk.API(access_token=self.api_token, v=self.api_version).users.search(count=1000, hometown=self.city, fields=['sex', 'bdate'])
    
    def api_json_to_df(self):
        """
        Transform json to df and clean data

        Returns:
        - pd.DataFrame: A DataFrame with information about users
        """
        res_data = self.api_query_1000()
        df = pd.DataFrame(res_data['items'])[['id', 'first_name', 'last_name', 'bdate', 'sex']]
        df.sex = df.sex.apply(gender_from_int)
        df.bdate = df.bdate.apply(year_from_bdate)
        df.bdate.fillna(np.nan, inplace = True)

        return df